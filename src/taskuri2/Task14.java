package taskuri2;
/*
Write an application, that will read two positive integers and will write their greatest common divisor (GCD) using Euclid's algorithm with modulo variation. Greatest common divisor (GCD) or two numbers is the biggest natural number, that both numbers are divisible by.

For example, GCD of 6 and 16 equals 2, because it's the smallest number that both 6 is divisile by (giving result 3) and 16 is divisible by (giving result 8).
 */
import java.util.Scanner;
public class Task14 {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        System.out.println("First number divisor: ");
        int a = scanner.nextInt();
        System.out.println("Second number dividend");
        int b = scanner.nextInt();

        while (b != 0) {
            int c = a % b;
            a = b;
            b = c;
        }

        System.out.println(a);
    }
}
