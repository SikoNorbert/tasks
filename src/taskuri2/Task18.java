package taskuri2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/*
Write an application, that will read a date in standard format of yyyy-MM-dd and will print the date of next Friday in the same format.
 */
public class Task18 {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        String date = "2020/11/09";
        LocalDate dateOriginal = LocalDate.parse(date,formatter);
        LocalDate dateNext = dateOriginal.plusDays(11);
        System.out.println(dateOriginal + " " + dateNext);
    }
}
