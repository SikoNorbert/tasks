package taskuri2;
/*
Write an application, that will read text (type String, representing web address) and change prefix from http:// to https://, but only if it's at the beginning of the text.
 */
import java.util.Scanner;
public class Task20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputAddress = scanner.nextLine();
        System.out.println(inputAddress.replace("http","https"));

    }
}
