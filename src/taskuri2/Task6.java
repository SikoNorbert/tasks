/*
Write an application, that will print the full latin alphabet (upper case letters) in alphabetical order. Each letter should be printed in new line
 */
package taskuri2;

public class Task6 {
    public static void main(String[] args) {
        System.out.println("Latin numbers in alphabetical order: ");
        char a;
        for(a = 'A'; a <= 'Z'; a++){
            System.out.println(a);
        }
    }
}
