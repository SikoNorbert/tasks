package taskuri2;
/*
Write an application that will read text and "bleep" swear words. Found swear words should be replaced with [bleep]. You can assume that swear words will be all lower case.

Swear words are: cheese and rice, chuck it, motherpuffin.

For example, text: It's not compiling again, cheese and rice!

Should be replaced with: It's not compiling again, [bleep]!
 */

public class Task12 {
    public static void main(String[] args) {
            System.out.println("The text below is");
            String[] text = { "I've broked the laptop, cheese and rice!", "The ball I chuck it, it's green.", "I hate mondays, motherpuffin" };
            System.out.println("The bleeping texts are: " + text[0].replace("cheese and rice", "[bleep]") +
                     " " +text[1].replace("chuck it", "[bleep]") +
                     " " +text[2].replace("motherpuffin", "[bleep]"));
    }
}
