package taskuri2;
/*
Write an application, that will read two times five numbers (ten variables of type int) and print only those, which occured both in first and second five-element set of input. For simplicity, you can assume that numbers will not repeat within one five-element set.
 */
import java.util.Scanner;

public class Task13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arr[][] = new int[2][5];

        // read numbers given by user to two-dimensional array
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 5; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }
        // numbers in array arr[0] is first five-element set
        // numbers in array arr[1] is second five-element set
        for (int i = 0; i < 5; i++) {
            int numberToFind = arr[0][i];
            for (int j = 0; j < 5; j++) {
                if (arr[1][j] == numberToFind) {
                    System.out.println(numberToFind);
                    break; // as we already found it, there's no reason to go on with this loop
                }
            }
        }
    }
}
