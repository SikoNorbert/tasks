package taskuri2;

public class Dog {
    private String name;
    private String wagtail;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWagtail() {
        return wagtail;
    }

    public void setWagtail(String wagtail) {
        this.wagtail = wagtail;
    }
    public void information(){
        System.out.println(this.name + this.wagtail);
    }
    public void informationAboutSparky() {
        System.out.println(this.name + this.wagtail);
    }
}
