/*
Write an application, that will draw Christmas tree according to examples below. The height of the tree should be read from user (positive integer).

Examples:

Tree with height=3:

  *
 ***
*****
  #
Tree with height=4:

   *
  ***
 *****
*******
   #
Your code should print a tree for any proper height given. Your application should generate the tree using single characters - it must not return tree copy-pasted by you!
 */
package taskuri2;

public class Task9 {

    public static final int SEGMENTS = 2;
    public static final int HEIGHT = 4;
    public static void main(String[] args){
        makeTree();
    }

    public static void makeTree(){
        int maxStars = 2*HEIGHT+2*SEGMENTS-1;
        String maxStr = "";
        for (int len=0; len < maxStars; len++){
            maxStr+=" ";
        }
        for (int i=1; i <= SEGMENTS; i++){
            for (int line=1; line <= HEIGHT; line++){
                String starStr = "";
                for (int j=1; j <= 2*line+2*i-3; j++){
                    starStr+="*";
                }
                for (int space=0; space <= maxStars-(HEIGHT+line+i); space++){
                    starStr = " " + starStr;
                }
                System.out.println(starStr);
            }
        }

        for (int i=0; i <= maxStars/2;i++){
            System.out.print(" ");
        }
        System.out.print("*\n");
        for (int i=0; i <= maxStars/2;i++){
            System.out.print(" ");
        }
        System.out.print("*\n");
        for (int i=0; i <= maxStars/2-3;i++){
            System.out.print(" ");
        }
        System.out.print("#\n");
    }
}