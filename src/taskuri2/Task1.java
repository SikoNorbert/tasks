/*
Write an application, that will calculate speed in kilometers per hour (km/h) equal to given speed in miles per hour (mph). Your application should read one variable from user (of type float) and calculate speed, using following formula:

speed in km/h = 1.6 * speed in mph
 */
package taskuri2;
import java.util.Scanner;
public class Task1 {
    public static void main(String[] args) {
        Scanner speed = new Scanner(System.in);
        System.out.println("Speed in mph is: ");
         float speedInMpH = speed.nextFloat();
         double speedInKmH = speedInMpH * 1.6;
        System.out.println(speedInKmH);
    }
}
