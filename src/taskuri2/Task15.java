package taskuri2;
/*
Write an application, that will read two positive numbers (type int) and will return a remainder after division of the bigger number by smaller number.

Warning! To make it harder, your application MUST NOT use neither modulo operation (%) nor division (/).
 */
import java.util.Scanner;
public class Task15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("First number: ");
        int a = scanner.nextInt();
        System.out.println("Second number: ");
        int b = scanner.nextInt();

        int bigger, smaller;
        if (a > b) {
            bigger = a;
            smaller = b;
        } else {
            bigger = b;
            smaller = a;
        }

        while (bigger >= smaller) {
            bigger -= smaller;
        }

        System.out.println(bigger);
    }
}
