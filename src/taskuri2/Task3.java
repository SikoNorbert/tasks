/*
Write an application, that will read two numbers from user (of type int) and will print true, if both numbers are the same sign (both are positive, or both are negative), or false otherwise. If at least one of given numbers is equl to 0, your application should print false.
 */
package taskuri2;
import java.util.Scanner;
public class Task3 {
    public static void main(String[] args) {
        Scanner numbers = new Scanner(System.in);
        int a = numbers.nextInt();
        int b = numbers.nextInt();
        if(a > 0 && b > 0){
            System.out.println("true");
        }
        if(a < 0 && b < 0){
            System.out.println("True");
        }
        else {
            System.out.println("false");
        }
        if(a == 0 || b == 0){
            System.out.println("False");
        }
    }
}
