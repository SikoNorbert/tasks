/*
Write an application that will read text (type String) and check if given text is a palindrome. Your application should print true or false.

Palindrome is a text, that reads the same backwards as forwards. It is case-insensitive (doesn't matter if letter is lowercase or uppercase) and ignores spaces.

Example of palindrome: Never odd or even.
 */
package taskuri2;
import java.util.Scanner;
public class Task10 {
    public static void main(String[] args) {
        String original, reverse = "";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Palindrom text: ");
        original = scanner.nextLine();
        int length = original.length();
        for (int i = length - 1; i >= 0; i--) {
            reverse = reverse + original.substring(original.length() - 1) + original.substring(1);
         }
        int i = 0;
            if (original.charAt(i) == reverse.charAt(i)) {
                System.out.println("TRUE");
            } else {
                System.out.println("FALSE");
            }
      }
 }