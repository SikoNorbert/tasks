/*
Write an application, that will read one number from user (of type int) and check, if given number is "near" 100. A number is "near" 100 when difference between it and 100 is no bigger than 10.
 */
package taskuri2;
import java.util.Scanner;
public class Task2 {
    public static void main(String[] args) {
        Scanner inputNumber = new Scanner(System.in);
        System.out.println("Random number: ");
        int number = inputNumber.nextInt();
        int differenceNumber = 100;
        if ((number - differenceNumber) < 10) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
    }
}