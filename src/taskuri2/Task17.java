package taskuri2;
/*
Write a method that will take a variable number of arguments (all are ints) and return their average value (float). Do not modify main method - you should only write method calculateAverage.
 */
import java.util.Scanner;
public class Task17 {
    public static void main(String[] args) {
        calculateAverage(20, 40, 60, 80, 100, 120);
        calculateAverage(1,2,3,4,5,6);
        calculateAverage(5,55,555,5555,55555,10000);
        calculateAverage(10,100,1000,10000,100000,1000000);
    }
    public static void calculateAverage(int number, int number1, int number2, int number3, int number4, int number5){
        float n;
        System.out.println("Average number method: ");
         n = (int) ((number + number1 + number2+ number3+ number4+ number5) / 6);
        System.out.println("Caculate average n: " + n);
    }
}
