/*
Write an application, that will read one floating-point number (of type float) and will print:

it's value rounded down to closest integer,
it's value rounded according to rules of math,
it's value rounded up to closest integer
 */
package taskuri2;
import java.util.Scanner;
public class Task5 {
    public static void main(String[] args) {
        Scanner floating = new Scanner(System.in);
        System.out.println("Rounding are's :");
        float number = floating.nextFloat();
        rounding(number);
    }
    public static void rounding(float number){
        System.out.println(Math.round(number));
        System.out.println(Math.ceil(number));
        System.out.println(Math.floor(number));
    }
}
