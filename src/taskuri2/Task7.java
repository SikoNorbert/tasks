/*
Write an application, that:

will read start number x (type int)
will read end number y (type int)
will check if start number is no bigger than end number and if both numbers are in range from 0 (included) to 10 000 (included). If not, your application will quit, without printing anything.
will write all integers divisible by 7 in given range (from x (included) to y (included)).
 */
package taskuri2;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;
public class Task7 {
    public static void main(String[] args) {
        Scanner scannerNumbers = new Scanner(System.in);
        System.out.println("Start number: ");
        int startNumber = scannerNumbers.nextInt();
        System.out.println("End number: ");
        int endNumber = scannerNumbers.nextInt();
        int j = 0;

        for(int i = startNumber; i <= endNumber; i++){
            if(startNumber < endNumber && (startNumber >= 0 && startNumber <=10000) && (endNumber >=0 && endNumber <=10000)){
            }
            if(i % 7 == 0) {
                j++;
                System.out.println(i + " " + j);
            }
        }
    }
}
