package taskuri2;
/*
Write an application, that will read text (type String) and will upper-case letters in last three characters of text. If text is shorter than three characters, your application should upper-case all letters.

Examples:

Lorem ipsum -> Lorem ipSUM
Lorem ipsum:) -> Lorem ipsuM:)
ok -> OK
 */

import java.util.Scanner;
public class Task11 {
    public static void main(String[] args) {
       Scanner keyword = new Scanner(System.in);
        System.out.println("Text:");
       String word = keyword.nextLine();
        String capitalizedLetter = word.substring(word.length()-3,word.length()).toUpperCase();
        String newWord = word.substring(0, word.length()-3) + capitalizedLetter;
        System.out.println(newWord);
    }
}