package taskuri2;
/*
Finish given application. Your application:

should compile without errors,
should write following text:
Fido wags tail.
Sparky wags tail.
 */
public class Task19 {
    public static void main(String[] args) {
         Dog dog = new Dog();
         dog.setName("Fido" + " ");
         dog.setWagtail("wags tail");

         dog.information();

        Dog sparky = new Dog();
        sparky.setName("Sparky" + " ");
        sparky.setWagtail("wags tail");
        sparky.informationAboutSparky();
    }
}
