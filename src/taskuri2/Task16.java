package taskuri2;
/*
Write an application, that will read a positive integer and will print it's factorial.

Factorial of n equals: n! = 1 * 2 * ... * n

Examples:

5! = 1 * 2 * 3 * 4 * 5 = 120
3! = 1 * 2 * 3 = 6
 */
import java.util.Scanner;
public class Task16 {
    public static void main(String[] args) {
        System.out.println("Factorial numbers are: " + factorial(5));
    }
    public static int factorial(int n){
        Scanner scanner= new Scanner(System.in);
         n = scanner.nextInt();
        for(int i = 0; i <=n; i++){
            int[] b = new int[]{1,2,3,4,5};
            b[0] = 1;
            b[i] = b[0] * b[i+1] * b[i+2] * b[i+3] * b[i+4];
            return b[i];
        }
        return n;
    }
}
