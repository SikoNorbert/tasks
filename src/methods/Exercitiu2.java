package methods;

import java.util.Scanner;

public class Exercitiu2 {
    public static void main(String[] args) {
        int a = 90, b = 160, rezultat = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Afiseaza operatia: ");
        String operatieMatematica = scanner.nextLine();

       switch(operatieMatematica) {
           case "adunare" : {
               rezultat = suma(a, b);
               break;
           }
           case "scadere" : {
               rezultat = scadere(a, b);
               break;
           }
           case "inmultire" : {
               rezultat = inmultire(a, b);
               break;
           }
           case "impartire" : {
               rezultat = impartire(a, b);
               break;
           }
           case "cat" : {
               rezultat = cat(a, b);
               break;
           }
       }
    }
    static int suma(int a, int b) {
        return a+b;
    }
    static int scadere(int a, int b) {
        return a-b;
    }
    static int inmultire(int a, int b) {
        return a*b;
    }
    static int impartire(int a, int b) {
        return a/b;
    }
    static int cat(int a, int b) {
        return a%b;
    }
}
