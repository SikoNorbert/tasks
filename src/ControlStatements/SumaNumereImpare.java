package ControlStatements;

import java.util.Scanner;

public class SumaNumereImpare {
    public static void main(String[] args) {
        System.out.println("Suma numere impare pana la n");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti numar de la tastatura");
        int n = scanner.nextInt();
        int sumaNrImpare = 0;
        for (int i = 0; i <= n; i++) {
            if (i % 2 == 1) {
                 sumaNrImpare = sumaNrImpare + i;
            }
        }
        System.out.println("Suma numarelor impare este:" + sumaNrImpare);
    }
}
