package ControlStatements;

import java.util.Scanner;

public class AfisareAniBisecti {
    public static void main(String[] args) {
        System.out.println("Introduceti anul");
        Scanner scanner = new Scanner(System.in);
        int anulCurent = scanner.nextInt();
        for(int contor = 1; contor <= anulCurent; contor++){
            if((contor % 4 == 0) && (contor % 100 != 0) || (contor % 400 == 0)) {
                System.out.println("Acesta este an bisec " + contor);
            }
        }
    }
}
