package ControlStatements;

import java.util.Scanner;

public class SumaNumerePareImpare {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdu un numar: ");
        int m = scanner.nextInt();
        int sumaNumerelorPare =0, sumaNumerelorImpare = 0;
        for(int contor = 1; contor <= m; contor++){
              if(contor % 2 == 0){
                   sumaNumerelorPare += contor;
              } else {
                  sumaNumerelorImpare += contor;
              }
        }
        System.out.println(sumaNumerelorPare);
        System.out.println(sumaNumerelorImpare);
    }
}
