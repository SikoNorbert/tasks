package ControlStatements;

public class SwitchTest {
    public static void main(String[] args) {
        int x, y, rezultat;
        x = 10;
        y = 15;
        rezultat = x - y;
        switch(rezultat) {
            case 3:
                System.out.println(y);
                break;
            case 9:
                System.out.println(x);
                break;
            case -5:
                System.out.println(x+y);
                break;
            default:
                System.out.println("sorry...try again");
                break;
        }
    }
}
