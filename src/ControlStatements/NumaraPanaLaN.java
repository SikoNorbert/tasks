package ControlStatements;

import java.util.Scanner;

public class NumaraPanaLaN {
    public static void main(String[] args) {
        int n;
        System.out.println("Scriere de la tastatura: ");
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        for(int contor = 1; contor <= n; contor+=5) {
            System.out.println(contor);
        }

    }
}
