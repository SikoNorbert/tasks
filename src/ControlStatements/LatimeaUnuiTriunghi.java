 package ControlStatements;

 import java.util.Scanner;

 public class LatimeaUnuiTriunghi {
        public static void main(String[] args) {
            int a, b, c;

            System.out.println("Initializam pe a: ");
            Scanner scanner1 = new Scanner(System.in);
            a = scanner1.nextInt();
            System.out.println("Initializam pe b: ");
            Scanner scanner2 = new Scanner (System.in);
            b = scanner2.nextInt();
            System.out.println("Initializam pe c: ");
            Scanner scanner3 = new Scanner (System.in);
            c = scanner3.nextInt();

            if( (a > 0 && b > 0 && c > 0) && ( a + b > c) && ( a + c > b) && ( b + c > a)) {
                System.out.println("Avem un triunghi");
            } else {
                System.out.println("Nu avem triunghi");
            }
        }
    }

