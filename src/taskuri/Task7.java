/*
Write a program that takes a positive integer N - n (int) from the user, and then displays all powers of the number 2 in the console that are not greater than the number given (each number should be displayed in new line.

For example, for the number 71 the program should write in the console the numbers: 1 2 4 8 16 32 64, in new line each.
 */
package taskuri;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("We assign a power number: \n");
        int n = scanner.nextInt();
        int power = 0, powerN;
        do {
            powerN = (int) Math.pow(2,power);
            power++;
            System.out.println("Displaying the power of number 2 when the power is not greater than the number:"
                    + powerN + " is the number");
        }
        while(powerN < n);

    }
}
