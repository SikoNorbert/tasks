/*
Write a program that, based on the variable: income (double), will calculate the amount of personal income tax due and write it to the console.

The tax is calculated according to the following rules:

up to 85,528.00 tax is 18% of the base minus 556.02,
from 85,528.00 tax is 14,839.02 + 32% of the surplus over 85,528.00.
 */
package taskuri;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("I am initializing the income: \n");
        double income = in.nextDouble();
        double tax;
        if(income <= 85528.0){
            tax = income * 0.18 - 556.02;

            if(tax < 0){
                tax = 0;
            }
            System.out.println("Impozitul meu este " + tax);
        } else {
            tax = 14849.02 + 0.32 * (income - 85528.0);
            System.out.println("In caz ca depasesc taxa propusa, atuncia taxa mea va fi calculata: " + tax);
        }
    }
}
