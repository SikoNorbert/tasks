/*
Write a program that takes a positive integer from the user - number (int), and then prints all positive odd numbers not greater than the given number in the console in order, each number in new line.
For example, for the number 15, the program should write in the console the numbers: 1, 3, 5, 7, 9, 11, 13, 15 (in new line each)
 */

package taskuri;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("I am choosing an random number: \n");
        int number = scanner1.nextInt();
        int checkOddNumbers = 0;
        for(int i = 1; i <= number; i++) {
            if(i % 2 == 1) {
                checkOddNumbers = checkOddNumbers + i;
                System.out.println("The odd numbers of an integer is: " + checkOddNumbers + "\n");
            }

        }
    }
}
