/*
Write a program that reads the text entered by the user and then divides it into individual words. Then counts the number of occurrences of words regardless of case and writes them to the console in alphabetical order.

For example, for the text "Ala likes cats, but she is not liked by the Cats."
 */
package taskuri;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task15 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Text inputted is: ");
        String text = input.nextLine();
            String[] in = (text.split(" "));
            for(String text2 : in){
                System.out.println(text2);
            }
        int i = 0;
        Pattern p = Pattern.compile(" cats");
        Matcher m = p.matcher( text );
        while (m.find()) {
            i++;
        }
        System.out.println(i);
    }
}
