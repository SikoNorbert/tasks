/*
  Write a program that based on the variable temperature in degrees Celsius - tempInCelsius (float), will calculate the temperature in degrees Farhenheit (degrees Fahrenheit = 1.8 * degrees Celsius + 32.0) and write it in the console.
 */
package taskuri;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
    Scanner temp = new Scanner(System.in);
    float tempInCelsius = temp.nextFloat();
        System.out.println("I am calculating the temperateure in degrees Fahrenheit: \n");
    float Fahrenheit = 1.8f * tempInCelsius + 32.0f;
        System.out.println("The Fahrenheit degrees conversion from Celsius is: " + Fahrenheit + "degree");
  }
}
