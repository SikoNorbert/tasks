/*
Write a program that takes numbers from the user as long as the number 0 is not given. When the number 0 is given, the program calculates the sum of the numbers given and prints it in the console.

For example, for a series of given numbers: 3, 2, 5, 1, 0, the program should write the number 11
 */
package taskuri;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        int sum = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("My first number is: \n");
        int a = scanner.nextInt();
        System.out.println("My second number is: \n");
        int b = scanner.nextInt();
        System.out.println("My third number is: \n");
        int c = scanner.nextInt();
        System.out.println("My fourth number is: \n");
        int d = scanner.nextInt();
        System.out.println("My fifth number is: \n");
        int e = scanner.nextInt();
        System.out.println("My sixth number is: \n");
        int f = scanner.nextInt();
        if(a==0 || b==0 || c==0 || d==0 || e==0 || f==0){
            sum = sum + a + b + c + d + e + f;
            System.out.println(sum);
        } else {
            System.out.println("All the number is greater than 0");
        }
    }
}
