package taskuri;

import java.util.Scanner;
import java.util.Stack;

public class Task17 {
    public static void main(String[] args) {
        Scanner text = new Scanner(System.in);
        System.out.println("The text below represents brackets correction: ");
        String brackets = text.nextLine();

        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < brackets.length(); i++) {
            char current = brackets.charAt(i);
            if (current == ')') {
                stack.push(current);
            }
            if ( current == '(') {
                char last = stack.peek();
                if (current == '(' && last == ')') {
                    stack.pop();
                }
            }
            if(stack.push(current) == stack.peek()){
                System.out.println("Brackets correct");
            } else {
                System.out.println("Incorrect brackets");
            }
        }
    }
}
