/*
  Write a program that, based on the variables: height - height (int) and weight - weight (float), checks whether a person can ride a roller coaster.
If a person is taller than 150cm and does not exceed 180kg, the program will write in the console "Fasten your seatbelt!", Otherwise it will write in the console "I'm sorry you can't go!".
Get the data from the user in the console using the Scanner class.
 */
package taskuri;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        int height;
        float weight;
        Scanner h = new Scanner(System.in);
        System.out.println("The height of a person is: \n");
        height = h.nextInt();
        Scanner w = new Scanner(System.in);
        System.out.println("The weight of a person is: \n");
        weight = w.nextFloat();
        if(height <= 150 || weight >= 180.0) {
            System.out.println("I'm sorry you can't go! \n");
        } else {
            System.out.println("Fasten your seatbelt! ");
        }
    }
}
