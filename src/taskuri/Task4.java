/*
Write a program which, based on the variables: amount - amount (double) and number of installments - numberOfInstallments (int), will calculate the monthly loan installment and write it to the console. The parameters have restrictions:

the loan amount must be between 100.00 and 10,000.00,
the number of installments must be between 6 and 48.
If the loan amount exceeds the acceptable range, the loan amount should be set at 5,000.00. If the number of installments exceeds the acceptable range, the number of installments should be set to 36.

The calculated monthly installment should also include interest. To simplify the calculations, assume that you add X percent to the loan amount depending on the number of installments:

6-12 installments - 2.5%,
13-24 installments - 5.0%,
25-48 installments - 10.0%,
and then calculate the installment amount based on the number of installments.
 */

package taskuri;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        double loan;
        int installments;
        double monthlyInstallments= 0.0;
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("My amount is: \n");
        double amount = scanner1.nextDouble();
        Scanner scanner2 = new Scanner(System.in);
        System.out.println("My loan from others is: \n");
        loan = scanner2.nextDouble();
        Scanner scanner3 = new Scanner(System.in);
        System.out.println("My installments is: \n");
        installments = scanner3.nextInt();
        if(amount > 10000.0) {
             amount = 5000.0;

            if(installments > 48){
                installments = 36;
            }
            if(installments >=6 || installments <=12){
                monthlyInstallments = 0.025;
            }
            if(installments >=13 || installments <=24){
                monthlyInstallments =  0.050 ;
            }
            if (installments >=25 || installments <=48){
                monthlyInstallments = 0.10;
            }

        }
        System.out.println("My amount are in the interval? " + amount);
        System.out.println("What are the installments between? " + installments);
        System.out.println("My installments depending on loan is: " + (amount* (1.0 + monthlyInstallments)/installments) + "installments");
    }
}
