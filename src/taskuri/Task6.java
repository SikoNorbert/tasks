/*
Write a program that takes from the user two integers A - a (int) and B - b (int), where A <B, and then calculates the sum of the sequence of numbers from A to B (A, A + 1, A + 2, ..., B) and prints it in the console. When the A <B condition is not met, the program exits by writing "Job completed" in the console.

For example, for A = 4 and B = 11, the program should write the number 60 in the console.
 */
package taskuri;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Assign a value to a: \n");
        int a = scanner.nextInt();
        System.out.println("Assign a value to b: \n");
        int b = scanner.nextInt();
        if (b <= a) {
            System.out.println("Job completed");
        } else {
            int sum = 0;
            for (int i = a; i <= b; i++) {
                sum += i;
            }
            System.out.println(sum);
        }

    }
}
