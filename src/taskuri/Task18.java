package taskuri;
import java.util.Scanner;

public class Task18 {
    public static void main(String[] args) {
        Scanner inputString = new Scanner(System.in);
        System.out.println("Introduce text in area: \n");
        String lowercases = inputString.nextLine().toLowerCase();
        System.out.println("Enter the value by which each character in the plaintext messages get shifted: \n");
        int shift = inputString.nextInt();
        String decryptMessage = "";
        for(int i=0; i < lowercases.length();i++)

        {
            // Shift one character at a time
            char alphabet = lowercases.charAt(i);
            // if alphabet lies between a and z
            if(alphabet >= 'a' && alphabet <= 'z')
            {
                // shift alphabet
                alphabet = (char) (alphabet - shift);

                // shift alphabet lesser than 'a'
                if(alphabet < 'a') {
                    //reshift to starting position
                    alphabet = (char) (alphabet-'a'+'z'+1);
                }
                decryptMessage = decryptMessage + alphabet;
            }

        }
        System.out.println(" decrypt message : " + decryptMessage);
    }
}
