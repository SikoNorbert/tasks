package taskuri;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Adding an integer number: \n");
        int n = scanner.nextInt();
        for(int i = 1; i <= n; i++){
            if(n % i == 0){
                System.out.println("Divided numbers are: " + i);
            }
        }
    }
}
