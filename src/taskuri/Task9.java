/*
Write a program that retrieves an integer string from the user. Downloading data ends with the number 0 (not included in the data). Then, the program calculates the sum of the largest and smallest of the given numbers and their arithmetic average and prints them in the console.

For example, for a series of given numbers: 1, -4, 2, 17, 0, the program should write in the console the numbers: 13, 6.5.
 */
package taskuri;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("My first variable is : \n");
        int a = scanner.nextInt();
        System.out.println("My second variable is: \n");
        int b = scanner.nextInt();
        System.out.println("My third variable is: \n");
        int c = scanner.nextInt();
        System.out.println("My fourth variable is : \n");
        int d = scanner.nextInt();
        System.out.println("My last variable is : \n");
        int e = scanner.nextInt();
        int min=0, max=0, sumGreatAndLess;
        double arithmeticAverage;
        if((a!=0 && b!=0 && c!=0 && d!=0) || (e==0)){
            if((a < 0)){
                min = a;
            }
            if((b < 0)){
                min = b;
            }
            if((c < 0)){
                min = c;
            }
            if((d < 0)){
                min = d;
            }
            if((a > 0)) {
                max = a;
            }
            if((b > 0)) {
                max = b;
            }
            if((c > 0)) {
                max = c;
            }
            if((d > 0)) {
                max = d;
            }
            sumGreatAndLess = min + max;
            System.out.println("The sum of the greater and lesser number is: " + sumGreatAndLess + " number");
        }
        arithmeticAverage = (double)(Math.abs(a) + Math.abs(b) + Math.abs(c) + Math.abs(d) + e)/5;
        System.out.println(" The arithmetics of these numbers is: " + arithmeticAverage + " number");
    }
}
