/*
Write a program that takes an integer greater than 1 from the user and checks if the number is a prime number. In the case when this number is a prime number, the program will write a message "Yes" in the console, otherwise it will write a message "No" in the console.

If the user gives a number less than or equal to 1, the program will write in the console the message: "I am interrupting work"
 */
package taskuri;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Adding and integer number: \n");
        int number = scanner.nextInt();
        int prim = 0;
        for(int i = 2; i <= Math.sqrt(number); i++){
            if(number % i == 0){

                System.out.println("No!" + i + "\n");
            }
             if(number % i != 0) {
                 prim = prim + i;

                System.out.println("Yes!" + prim + "\n");
            }
            if(number < 0 || number == 1){
                System.out.println("I am interrupting work.");
            }
        }
    }
}
