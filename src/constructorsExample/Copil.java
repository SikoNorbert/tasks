package constructorsExample;

public class Copil {
    private String nume;
    private String prenume;
    private double inaltime;
    private int varsta;
    private double greutate;

    public Copil() {

    }
    public Copil(String nume, String prenume, double inaltime, int varsta){
        this.nume=nume;
        this.prenume=prenume;
        this.inaltime=inaltime;
        this.varsta=varsta;
    }
    public Copil(String nume, String prenume) {
        this.nume=nume;
        this.prenume=prenume;
    }
    public Copil(String nume, String prenume, double inaltime) {
        this.nume=nume;
        this.prenume=prenume;
        this.inaltime= inaltime;
    }
    public Copil(String nume, String prenume, int varsta) {
        this.nume=nume;
        this.prenume=prenume;
        this.varsta=varsta;
    }
    public Copil(double greutate, String nume, String prenume) {
        this.greutate=greutate;
        this.nume=nume;
        this.prenume=prenume;
    }
}
