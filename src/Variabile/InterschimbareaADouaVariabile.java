package Variabile;

public class InterschimbareaADouaVariabile {
    public static void main(String[] args) {
            int a = 5, b = 4;

            System.out.println(a + " " + b);
            interschimbareVariabile(a,b);
            interschimbareVariabileV2(99, 487);
            interschimbareVariabileV3(555, 777);


    }
    static void interschimbareVariabile(int x, int y) {
        int auxiliar;
        auxiliar = x; // auxiliar =5, a=5, b=4
        x = y; // a=4, auxiliar = 5, b = 4
        y = auxiliar; // a=4, auxiliar = 5, b=5
        System.out.println(x + " " + y);
    }
    static void interschimbareVariabileV2(int x, int y){
        x = x + y; // x = 5 + 4 = 9
        y = x - y; // y = 9 - 4 = 5
        x = x - y; // x = 9 - 5 = 4
        System.out.println(x + " " + y);
    }
    static void interschimbareVariabileV3(int x, int y){
        x = x * y; // x = 5 * 4 = 20
        y = x / y; // y = 20 / 4 = 5
        x = x / y; // x = 20 / 5 = 4
        System.out.println(x + " " + y);
    }
}
