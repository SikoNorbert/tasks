package classesAndObjects;

public class ExecutieMasina {
    public static void main(String[] args) {
        // clasa nume_obiect = new clasa()
        Masina masinaLuiDenis = new Masina();
        masinaLuiDenis.afiseazaVitezaMaximaMasina();

        System.out.println("Am Pornit la Drum");
        masinaLuiDenis.accelerareMasina();
        masinaLuiDenis.accelerareMasina();
        masinaLuiDenis.decelerareMasina();
        masinaLuiDenis.accelerareMasina();
        masinaLuiDenis.accelerareMasina();

        masinaLuiDenis.aiCalatorit100kmPrinAfaraOrasului();
        masinaLuiDenis.aiCalatorit100kmPrinAfaraOrasului();

        masinaLuiDenis.aiCalatorit100kmInOrasului();

        double combustibilRamas = masinaLuiDenis.returneazaCatCombustibilAMaiRamasInMasina();

        System.out.println("Mai ai " + combustibilRamas + "litri in motorul de rezervor");
        if (combustibilRamas > 10) {
            System.out.println("Stai linistit, mai ai combustibil pentru o calatorie!");
        } else {
            System.out.println("Du-te si alimenteaza rapid!");
        }

    }
}
