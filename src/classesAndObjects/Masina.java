package classesAndObjects;

public class Masina {
    String marcaMasina;
    String culoare;
    double pretAchizitie;
    int capacitateCilindrica;
    int nrLocuriPasageri;
    int greutateMasina;
    int anFabricatie;
    int vitezaInitiala = 0;
    int vitezaMaxima = 250;
    double consumCombustibilInAfaraOrasului = 3.4;
    double consumCombustibilInInteriorulOrasului = 4.3;
    double capacitateRezervorCombustibil = 54.7;

    void afiseazaVitezaMaximaMasina() {
        System.out.println("Viteza maxima a masinii este" + vitezaMaxima + "km/h");
    }

    void accelerareMasina() {
        System.out.println("Tocmai ai apasat pe acceleratie!");
        vitezaInitiala = vitezaInitiala + 10;
        capacitateRezervorCombustibil--;
        System.out.println("Viteza actuala este de " + vitezaInitiala + "km/h");
    }

    void decelerareMasina() {
        System.out.println("Tocmai ai apasat pe frana!");
        vitezaInitiala = vitezaInitiala - 10;
        System.out.println("Viteza actuala este de " + vitezaInitiala + "km/h");
    }

    void aiCalatorit100kmPrinAfaraOrasului() {
      capacitateRezervorCombustibil= capacitateRezervorCombustibil -consumCombustibilInAfaraOrasului;
    }
    void aiCalatorit100kmInOrasului() {
        capacitateRezervorCombustibil = capacitateRezervorCombustibil - consumCombustibilInInteriorulOrasului;
    }

    double returneazaCatCombustibilAMaiRamasInMasina(){
        return capacitateRezervorCombustibil;
    }
}
