package array;

public class BiDimensionalArray {
    public static void main(String[] args) {
        String[][] produseMagazin = new String[3][];
        produseMagazin[0] = new String[] {"Ciocolata", "Napolitane", "Biscuiti"};
        produseMagazin[1] = new String[] {"Paine graham", "Pain au chocolat", "Baguette", "gogosi", "pateoase cu margarina"};
        produseMagazin[2] = new String[] {};
        for(int i = 0; i < produseMagazin.length; i++){
            for(int j = 0; j < produseMagazin[i].length; j++){
                System.out.println(produseMagazin[i][j]);
            }
        }
    }
}
