package array;

public class ArrayDeclaration {
    public static void main(String[] args) {
        int[] vector;
        int[] numereLoto = {12,3,5,34,7,8};
        int[] numereLotoSpania = new int[8];
        String[] elevi = new String[4];
        elevi[0] = "Cristi";
        elevi[3] = "Florin";
        for(int i =0 ; i<elevi.length; i++) {
            System.out.println(elevi[i]);
        }

    }
}
